from django.urls import path

from .forms import LoginForm
from myproject.core import views as v

app_name = 'core'

urlpatterns = [
    path('', v.index, name='index'),
    path('dashboard/', v.dashboard, name='dashboard'),
    path('logout/', v.logout, name='logout'),
]