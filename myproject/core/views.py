from django.contrib.auth.decorators import login_required
from django.shortcuts import render
from django.contrib.auth.mixins import LoginRequiredMixin
from django.urls import reverse, reverse_lazy
from django.views.generic import  ListView
from django.db.models import Q
from myproject.coopervap.models import Cliente, Ligacoes
# def index(request):
#     return render(request, 'index.html')
class  IndexView(LoginRequiredMixin, ListView):

    model = Cliente
    template_name = 'coopervap/func/index.html'
    # paginate_by = 100

    def get_context_data(self, **kwargs):
        pendentes = Ligacoes.objects.filter( Q(situacao = '0') | Q(situacao = '1' ) | Q(situacao = '2' ) | Q(situacao = '3' ) | Q(situacao = '6' ))
        total = pendentes.count()
        print ("Qtd pendentes:",total)
        context = super(IndexView, self).get_context_data(**kwargs)
        context['total'] = total
        return context

    def get_queryset(self):
        return Cliente.objects.all().order_by('-id')

#@login_required
def dashboard(request):
    return render(request, 'dashboard.html')


def logout(request):
    return render(request, 'dashboard.html')
index = IndexView.as_view()