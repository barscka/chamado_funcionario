from django.db import models

from django.urls import reverse, reverse_lazy
from django.conf import settings


# Create your models here.
from myproject.accounts.models import User

#funcionarios da coopervap que ligam solicitando o suporte
class Cliente(models.Model):

    nome = models.CharField('Quem ligou', max_length=140)
    slug = models.SlugField('Identificador', max_length=100)
    ramal = models.CharField('Ramal', max_length=4, blank=True, null=True)
    created = models.DateTimeField('Criado em', auto_now_add=True)
    modified = models.DateTimeField('Modificado em', auto_now=True)

    class Meta:
        verbose_name = 'Cliente'
        verbose_name_plural = 'Clientes'
        ordering = ['nome']

    def __str__(self):
        return self.nome


class Ligacoes(models.Model):
    STATUS = (
        (0, 'Aberto'),
        (1, 'Aguardando'),
        (2, 'Cancelado'),
        (3, 'Pendente'),
        (5, 'Concluido'),
        (6, 'Encaminhado'),

    )
    #status do chamado
    data = models.DateField(null=True, auto_now_add=True)
    hora = models.TimeField(auto_now_add=True, blank=True) 
    situacao = models.IntegerField('Estado do chamado',choices=STATUS, default=0) 
    cliente = models.ForeignKey('coopervap.Cliente', verbose_name='Quem Ligou', on_delete=models.CASCADE)
    #adicionado o usuario que abriu o chamado como atendente
    atendente = models.ForeignKey(settings.AUTH_USER_MODEL, verbose_name='Atendente', on_delete=models.CASCADE)#, blank=True, null=True)    # ramal tem que pegar da classe cliente (que abre o chamado)
    assunto = models.TextField('Resumo do Problema')
    solucao = models.TextField('Descrição detalhada da solução', blank=True)
    tecnico = models.ForeignKey('accounts.User', verbose_name='Tecnico',null=True, blank=True, related_name='Tecnico', on_delete=models.CASCADE)
    #campo a ser pensado no aplicativo, seria a data ou hora do fechamento do chamado
    fechado = models.DateTimeField('Fechado em', blank=True, null=True, )

    # controle de modificacoes

    created = models.DateTimeField('Criado em', auto_now_add=True)
    modified = models.DateTimeField('Modificado em', auto_now=True)

    class Meta:
        verbose_name = 'Chamado da Coopervap'
        verbose_name_plural = 'Chamados da Coopervap'

    # def __str__(self):
    #     return self.tipo or self.assunto

    def get_full_name(self):
        return str(self)

    def get_short_name(self):
        return str(self).split(" ")[0]

    def get_ordering(self, request):
        return [Lower('id')] 

    #def get_absolute_url(self):
        #return reverse('index', kwargs={'pk': self.pk})

