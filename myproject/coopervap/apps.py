from django.apps import AppConfig


class CoopervapConfig(AppConfig):
    name = 'coopervap'
