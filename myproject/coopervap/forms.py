from django.forms import ModelForm
from datetime import datetime
from django import forms
from crispy_forms.layout import Layout, Div, Submit, HTML, Button, Row, Field
from crispy_forms.bootstrap import AppendedText, PrependedText, FormActions
from django.conf import settings
import datetime
from bootstrap_datepicker_plus import DateTimePickerInput
from .models import Ligacoes, Cliente

class LigacoesForm(ModelForm):


    class Meta:
        model = Ligacoes
        data = forms.DateField(input_formats=settings.DATE_INPUT_FORMATS)
        fields = [
            #'num_chamado',
            'cliente', 'situacao', 'assunto', 'solucao'
             #'atendente'
            ]
class UpdateLigacoesForm(ModelForm):


    class Meta:
        model = Ligacoes
   
        
        fields = [
            'situacao', 'solucao','fechado', 'tecnico'
            ]
        widgets = {
       
            'fechado': DateTimePickerInput(format='%d/%m/%Y %H:%M'), # specify date-frmat
        }

class ClienteForm(forms.ModelForm):


    class Meta:
        model = Cliente
        fields = [
            'nome', 'slug', 'ramal',
            ]
#meu forms do problema do chamado cliente

class ChamadoClienteForm(ModelForm):

    class Meta:
        model = Ligacoes
        data = forms.DateField(input_formats=settings.DATE_INPUT_FORMATS)
        fields = [
            #'num_chamado',
            'cliente',  'assunto', 'solucao'
            #'atendente'
        ]
        widgets = {
            # cria um combobox para os clientes escondido (display=none)
            'cliente': forms.Select(attrs={'display': 'none'}),
        }
