from django.shortcuts import render, get_object_or_404
from django.views.generic import (
    CreateView, TemplateView, UpdateView, FormView, ListView, DetailView
)
from django.contrib import messages
from django.contrib.auth.mixins import LoginRequiredMixin
from django.db.models.functions import Lower
from django.urls import reverse, reverse_lazy
from django.core.mail import send_mail
from django.http import HttpResponse, HttpResponseRedirect
from django.contrib.auth.models import User
from django.db.models import Q
from django.conf import settings
from django.db import models
from fm.views import AjaxCreateView
from django.utils import timezone
#popup

from django.contrib.messages.views import SuccessMessageMixin
#gerar pdf
from easy_pdf.views import PDFTemplateView, PDFTemplateResponseMixin


# Create your views here.
from .models import Ligacoes, Cliente
from .forms import LigacoesForm, ClienteForm, ChamadoClienteForm, UpdateLigacoesForm

class IndexView(LoginRequiredMixin, ListView):

    model = Ligacoes
    template_name = 'coopervap/index.html'
    paginate_by = 100
       
    def get_queryset(self):
        queryset = Ligacoes.objects.all().order_by('-id')
        q = self.request.GET.get('q', '')
        if q:
            queryset = queryset.filter(
                models.Q(tipo__nome__icontains=q) | models.Q(
                    assunto__icontains=q)
                | models.Q(cliente__nome__icontains=q)
            )
        return queryset

    def get_context_data(self, **kwargs):
        pendentes = Ligacoes.objects.filter( Q(situacao = '0') | Q(situacao = '1' ) | Q(situacao = '2' ) | Q(situacao = '3' ) | Q(situacao = '6' ))
        total = pendentes.count()
        print ("Qtd pendentes:",total)
        context = super(IndexView, self).get_context_data(**kwargs)
        context['total'] = total
        return context

class LigacoesRegisterView(LoginRequiredMixin, CreateView):

    model = Ligacoes
    template_name = 'coopervap/register.html'
    form_class = LigacoesForm
    success_url = reverse_lazy('coopervap:index')

    def get_context_data(self, **kwargs):
        pendentes = Ligacoes.objects.filter( Q(situacao = '0') | Q(situacao = '1' ) | Q(situacao = '2' ) | Q(situacao = '3' ) | Q(situacao = '6' ))
        total = pendentes.count()
        print ("Qtd pendentes:",total)
        context = super(LigacoesRegisterView, self).get_context_data(**kwargs)
        context['total'] = total
        return context

    def post(self, request, *args, **kwargs):
            form = self.form_class(request.POST)
            if form.is_valid():


                form.revendedor = request.user
                obj = form.save(commit=False)
                obj.atendente = request.user
                
                obj.save()
                form.save()
                # <process form cleaned data>
                return HttpResponseRedirect(reverse('coopervap:index'))

            return render(request, self.template_name, {'form': form})

class PendenteListView(LoginRequiredMixin, ListView):

    model = Ligacoes
    template_name = 'coopervap/pendente.html'

    def get_context_data(self, **kwargs):
        pendentes = Ligacoes.objects.filter( Q(situacao = '0') | Q(situacao = '1' ) | Q(situacao = '2' ) | Q(situacao = '3' ) | Q(situacao = '6' ))
        total = pendentes.count()
        print ("Qtd pendentes:",total)
        context = super(PendenteListView, self).get_context_data(**kwargs)
        context['total'] = total
        return context


    def get_queryset(self):
        pendentes = Ligacoes.objects.filter( Q(situacao = '0') | Q(situacao = '1' ) | Q(situacao = '2' ) | Q(situacao = '3' ) | Q(situacao = '6' ))
        return pendentes

class FechadoListView(LoginRequiredMixin, ListView):

    model = Ligacoes
    template_name = 'coopervap/listafechado.html'

    def get_context_data(self, **kwargs):
        pendentes = Ligacoes.objects.filter( Q(situacao = '0') | Q(situacao = '1' ) | Q(situacao = '2' ) | Q(situacao = '3' ) | Q(situacao = '6' ))
        total = pendentes.count()
        print ("Qtd pendentes:",total)
        context = super(FechadoListView, self).get_context_data(**kwargs)
        context['total'] = total
        return context


    def get_queryset(self):
        return Ligacoes.objects.filter(situacao = '5')

class LigacoesDetailView(LoginRequiredMixin, DetailView):

    template_name = "coopervap/ochamado.html"

    def get_queryset(self):
        return Ligacoes.objects.filter()

class PdfDetailView(PDFTemplateResponseMixin, DetailView):
    model = Ligacoes 
    template_name = 'coopervap/pdf.html'

class LigacoesUpdateView(LoginRequiredMixin, UpdateView):

    model = Ligacoes
    template_name = 'coopervap/fechado.html'
    form_class = UpdateLigacoesForm
    success_url = reverse_lazy('core:index')

class TodosListView(LoginRequiredMixin, ListView):
    #lista todos os chamados
    model = Ligacoes
    template_name = 'coopervap/index-todos.html'
    #paginate_by = 10

    def get_queryset(self):
        return Ligacoes.objects.all().order_by('-id')

#app de cliente (funcionario da coopervap)
class ClienteList(LoginRequiredMixin, ListView):

    model = Cliente
    template_name = 'coopervap/func/index.html'
    # paginate_by = 100

    def get_context_data(self, **kwargs):
        pendentes = Ligacoes.objects.filter( Q(situacao = '0') | Q(situacao = '1' ) | Q(situacao = '2' ) | Q(situacao = '3' ) | Q(situacao = '6' ))
        total = pendentes.count()
        print ("Qtd pendentes:",total)
        context = super(FechadoListView, self).get_context_data(**kwargs)
        context['total'] = total
        return context

    # def get_context_data(self, **kwargs):
    #     qtdchamadoscli = Ligacoes.objects.filter(cliente__id=self.kwargs.get('pk'))
    #     totalchamadocli = qtdchamadoscli
    #     print("Qtd pendentes:", totalchamadocli)
    #     context = super(ClienteList, self).get_context_data(**kwargs)
    #     context['totalchamadocli'] = totalchamadocli
    #     return context

    def get_queryset(self):
        return Cliente.objects.all().order_by('-id')

class ClienteRegisterView(CreateView):

    model = Cliente
    template_name = 'coopervap/func/register.html'
    form_class = ClienteForm
    success_url = reverse_lazy('core:index')


class ClienteUpdateView(LoginRequiredMixin, UpdateView):

    model = Cliente
    template_name = 'coopervap/func/update.html'
    form_class = ClienteForm
    # fields = ['situacao', 'solucao']
    success_url = reverse_lazy('core:index')


class ClienteDetailView(LoginRequiredMixin, DetailView):

    template_name = "coopervap/func/ocliente.html"

    def get_queryset(self):
        return Cliente.objects.filter()

    def get_context_data(self, **kwargs):
        qtdchamadoscli = Ligacoes.objects.filter(cliente__id = self.kwargs.get('pk') )
        totalchamadocli = qtdchamadoscli
        print ("Qtd pendentes:",totalchamadocli)
        context = super(ClienteDetailView, self).get_context_data(**kwargs)
        context['totalchamadocli'] = totalchamadocli
        return context


#aqui e onde esta meu problema
class ChamadodoClienteRegisterView(FormView):

    model = Ligacoes
    template_name = 'coopervap/func/registerbyfunc.html'
    form_class = ChamadoClienteForm
    success_url = reverse_lazy('core:index')

    # class ChamadodoClienteRegisterView(CreateView):
    def get_context_data(self, **kwargs):
        # testando se o id da url é passado para esse contexto
        func_id = self.kwargs.get('item_id')
        teste_id = Cliente.objects.get(id=func_id)
        print("O id e: ", func_id, "e o outro e: ", teste_id.id)
        context = super(ChamadodoClienteRegisterView, self).get_context_data(**kwargs)
        # inicia o 'combobox cliente' com o usuario logado
        context['funcionario'] = self.form_class(initial={'cliente': Cliente.objects.get(id=func_id)})
        return context

    def post(self, request, *args, **kwargs):
        form = self.form_class(request.POST)
        if form.is_valid():
            obj = form.save(commit=False)
            obj.atendente = request.user
            obj.save()
            form.save()
            # <process form cleaned data>
            return HttpResponseRedirect(reverse('core:index'))

        return render(request, self.template_name, {'form': form})
#chamado
index = IndexView.as_view()
cadcoopervap = LigacoesRegisterView.as_view()
ochamado = LigacoesDetailView.as_view()
relatorio = PdfDetailView.as_view()
finalizando = LigacoesUpdateView.as_view()
pendentes = PendenteListView.as_view()
listafechado = FechadoListView.as_view()
#cliente
cadcliente = ClienteRegisterView.as_view()
clientelist = ClienteList.as_view()
alterarcliente = ClienteUpdateView.as_view()
ocliente = ClienteDetailView.as_view()
chamado_funcionario = ChamadodoClienteRegisterView.as_view()