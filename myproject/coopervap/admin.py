from django.contrib import admin
from import_export.admin import ImportExportModelAdmin
# Register your models here.
from .models import Ligacoes, Cliente

class LigacoesAdmin(ImportExportModelAdmin, admin.ModelAdmin):
    
    autocomplete_fields = ['cliente']
    list_display = ['id', 'data', 'created', 'modified']
    search_fields = ['id', 'cliente__slug', 'assunto']
    list_filter = ['created', 'modified', ]
    def __str__(self):
        return self.get_name_display()
  
    class Meta:
        model = Ligacoes
        widgets = {
                'data': {'format': '%d/%m/%Y'},
                }
        
        import_id_fields = ('id')
        fields = ('id', 'data', 'hora', 'cliente', 'atendente', 'assunto', 'solucao',)
        export_order = ('id', 'data', 'hora', 'cliente', 'atendente', 'assunto', 'solucao',)

class ClienteAdmin(ImportExportModelAdmin, admin.ModelAdmin):

    list_display = ['id', 'nome', 'slug', 'created', 'modified']
    search_fields = ['nome', 'slug',]
    list_filter = ['created', 'modified']

    class Meta:
        model = Cliente
        exclude = ('ramal' )
        import_id_fields = ('id')
        fields = ('id', 'nome' ,'slug', 'ramal')
        export_order = ('id', 'nome' ,'slug', 'ramal', 'created', 'modified')