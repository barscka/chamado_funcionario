from django.urls import path

from .views import index, cadcoopervap, cadcliente, clientelist, alterarcliente, ocliente, ochamado, relatorio, finalizando, pendentes, listafechado, chamado_funcionario

app_name = 'coopervap'

urlpatterns = [
    #chamados
    path('', index, name='index'),    
    path('ochamado/<int:pk>/', ochamado, name='ochamado'),
    path('relatorio/<int:pk>/', relatorio, name='relatorio'),
    path('finalizando/<int:pk>/', finalizando, name='finalizando'),    
    path('cadastro/', cadcoopervap, name='cadastro'),
    path('pendentes/', pendentes, name='pendentes'),
    path('fechado/', listafechado, name='listafechado'),
    #cliente
    path('quem-ligou/', cadcliente, name='cadcliente'),
    path('funcionarios/', clientelist, name='clientelist'),
    
    path('alterar-funcionario/<int:pk>/', alterarcliente, name='alterar-funcionario'),
    path('ocliente/<int:pk>/', ocliente, name='ocliente'),
    path('chamado-cliente/<int:item_id>/', chamado_funcionario, name='chamado_funcionario'),
  
] 