from django.urls import path
from django.contrib.auth import login, logout
from .views import index, register, update_user, update_password

app_name = 'accounts'

urlpatterns = [
    path('', index, name='index'), 
    path('cadastro', register, name='register'),    
    path('alterar-dados/', update_user, name='update_user'),
    path('alterar-senha/', update_password, name='update_password'),
]